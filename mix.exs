defmodule DistanceCalculator.MixProject do
  use Mix.Project

  def project do
    [
      app: :distance_calculator,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "MyApp",
      source_url: "https://gitlab.com/korczis/distance-calculator",
      homepage_url: "https://gitlab.com/korczis/distance-calculator",
      docs: [
        main: "DistanceCalculator", # The main page in the docs
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        :logger,
        :observer,
        :wx
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:csv, "~> 3.2"},
      {:ex_doc, "~> 0.31", only: :dev, runtime: false},
      {:jason, "~> 1.4"},
      {:tesla, "~> 1.8"}
    ]
  end
end
