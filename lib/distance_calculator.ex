defmodule DistanceCalculator do
  @moduledoc """
  Distance calculator

  Finds the distance between two coordinates
  """

  use Tesla

  require Logger

  @api_key "F9DZLlD7peo69Mc7ZamlIVrBsD64K-6rpaXPzg9oJ-c"

  plug(Tesla.Middleware.BaseUrl, "https://api.mapy.cz/v1")

  # Define an ETS table for caching
  def start_link() do
    :ets.new(:geocode_cache, [:set, :public, :named_table])
  end

  # Function to cache a geocoded address
  def cache_address(address, result) do
    :ets.insert(:geocode_cache, {address, result})
  end

  # Function to retrieve a cached geocoded address
  def get_cached_address(address) do
    :ets.lookup(:geocode_cache, address)
  end

  @doc """
  Geocodes address
  """
  def geocode_address(address) do
    case get_cached_address(address) do
      [{^address, result}] ->
        Logger.debug("Got cached address: #{inspect(address)}, result: #{inspect(result)}")
        result

      _ ->
        result = fetch_geocode_address(address)

        Logger.debug(
          "Address not cached, geocoding: #{inspect(address)}, result: #{inspect(result)}"
        )

        cache_address(address, result)
        result
    end
  end

  # Helper function to fetch geocode address from the API
  defp fetch_geocode_address(address) do
    query = %{
      "lang" => "cs",
      "query" => address,
      "apiKey" => @api_key,
      "limit" => 1,
      "format" => "json"
    }

    url = "/geocode?#{URI.encode_query(query)}"

    {:ok, %{body: body}} = get(url)
    IO.inspect(body)

    {:ok, %{items: items}} = Jason.decode(body, keys: :atoms)

    List.first(items)
  end

  @doc """
  Finds the route between two geo coordinates
  """
  def find_route({lat1, lon1}, {lat2, lon2}) do
    query = [
      start: lon1,
      start: lat1,
      end: lon2,
      end: lat2,
      lang: "cs",
      apiKey: @api_key,
      format: "geojson",
      routeType: "car_fast"
    ]

    url = "/routing/route?#{URI.encode_query(query)}"

    {:ok, %{body: body}} = get(url)
    {:ok, _} = Jason.decode(body, keys: :atoms)
  end

  @doc """
  Gets route length between two geo coordinates
  """
  def get_route_length({_lat1, _lon1} = coord_from, {_lat2, _lon2} = coord_to) do
    {:ok, %{length: length}} = DistanceCalculator.find_route(coord_from, coord_to)
    length * 0.001
  end

  @doc """
  Calculates distance between two geo coordinates
  """
  def get_haversine_distance({lat1, lon1}, {lat2, lon2}) do
    earth_radius_km = 6371.0
    d_lat = to_rad(lat2 - lat1)
    d_lon = to_rad(lon2 - lon1)

    a =
      :math.sin(d_lat / 2) ** 2 +
        :math.cos(to_rad(lat1)) * :math.cos(to_rad(lat2)) * :math.sin(d_lon / 2) ** 2

    c = 2 * :math.atan2(:math.sqrt(a), :math.sqrt(1 - a))
    earth_radius_km * c
  end

  def process_item(from, from_address, to, to_address) do
    %{position: %{lat: from_lat, lon: from_lon}, location: from_location} = from_address
    from_coords = {from_lat, from_lon}

    %{position: %{lat: to_lat, lon: to_lon}, location: to_location} = to_address
    to_coords = {to_lat, to_lon}

    distance_haversine = get_haversine_distance(from_coords, to_coords)
    distance_route = get_route_length(from_coords, to_coords)

    %{
      "from" => from,
      "from.location" => from_location,
      "from.lat" => from_lat,
      "from.lon" => from_lon,
      "to" => to,
      "to.location" => to_location,
      "to.lat" => to_lat,
      "to.lon" => to_lon,
      "distance.haversine" => distance_haversine,
      "distance.route" => distance_route
    }
  end

  @doc """
  Processes CSV file.

  ## Examples

      iex> DistanceCalculator.process_csv("data/example.csv")
      :ok

  """
  def process_csv(path) do
    base_name = Path.basename(path)
    out_name = String.replace(base_name, ".csv", "-out.csv")
    out_path = String.replace(path, base_name, out_name)
    file_out = File.open!(out_path, [:write, :utf8])

    File.stream!(path, [read_ahead: 1000], 1000)
    |> CSV.decode!(headers: true)
    |> Stream.map(fn %{"to" => to} ->
      from_address = geocode_address("613 00")
      to_address = geocode_address("#{to}, Czech Republic")

      case {from_address, to_address} do
        {nil, _} ->
          nil

        {_, nil} ->
          nil

        {from_address, to_address} ->
          process_item("613 00", from_address, to, to_address)
      end
    end)
    |> Stream.reject(&is_nil/1)
    |> CSV.encode(
      headers: [
        "from",
        "from.location",
        "from.lat",
        "from.lon",
        "to",
        "to.location",
        "to.lat",
        "to.lon",
        "distance.haversine",
        "distance.route"
      ]
    )
    |> Stream.each(&IO.write(file_out, &1))
    |> Stream.run()
  end

  @doc """
  Converts degrees to radians
  """
  def to_rad(deg) do
    deg * :math.pi() / 180
  end
end
